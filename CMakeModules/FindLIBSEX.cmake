# Searches libsex.
#
# Handles REQUIRED argument properly.
#
# The following variables will be set, if found:
# - LIBSEX_FOUND
# - LIBSEX_INCLUDE_DIR
# - LIBSEX_LIBRARIES



FIND_PATH(LIBSEX_INCLUDE_DIR  libsex/Exception.hxx ${CMAKE_SYSTEM_INCLUDE_PATH})
FIND_LIBRARY(LIBSEX_LIBRARIES sex                  ${CMAKE_SYSTEM_LIBRARY_PATH})

IF(LIBSEX_INCLUDE_DIR AND LIBSEX_LIBRARIES)
	SET(LIBSEX_FOUND true)
ENDIF(LIBSEX_INCLUDE_DIR AND LIBSEX_LIBRARIES)

IF(NOT LIBSEX_FOUND)
	IF(LIBSEX_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "libsex not found.")
	ELSE(LIBSEX_FIND_REQUIRED)
		MESSAGE(STATUS "libsex not found.")
	ENDIF(LIBSEX_FIND_REQUIRED)
ELSE(NOT LIBSEX_FOUND)
	MESSAGE(STATUS "Found libsex: ${LIBSEX_INCLUDE_DIR}, ${LIBSEX_LIBRARIES}.")
ENDIF(NOT LIBSEX_FOUND)
