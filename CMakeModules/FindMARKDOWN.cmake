# Searches MarkDown.
#
# Handles REQUIRED argument properly.
#
# The following variables will be set, if found:
# - MARKDOWN_FOUND
# - MARKDOWN_EXECUTABLE


FIND_PROGRAM(MARKDOWN_EXECUTABLE markdown)

IF(MARKDOWN_EXECUTABLE)
	SET(MARKDOWN_FOUND true)
ENDIF()

IF(MARKDOWN_FOUND)
	MESSAGE(STATUS "Found MarkDown: ${MARKDOWN_EXECUTABLE}")
ELSE()
	IF(MARKDOWN_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Required program MarkDown not found.")
	ELSE()
		MESSAGE(STATUS "MarkDown not found.")
	ENDIF()
ENDIF()
