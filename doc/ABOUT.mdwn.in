# @LIBRARY_NAME@: @PROJECT_BRIEF@

## about

@LIBRARY_NAME@ provides a simple exception-based assertion
mechanism that can be used to realize a rudimentary
design-by-contract policy.

It leverages [libsex][] to not only throw exceptions when
boolean expressions evaluate to false, but also to
declutter control flow by using macros that wrap new
exceptions around those thrown by substatements, creating a
concise backtrace that eases reconstruction of errors.


## news

* @PROJECT_VERSION_DATE@ @LIBRARY_NAME@-@PROJECT_VERSION@
* 2011-08-18 @LIBRARY_NAME@-0.1.0
* 2010-03-13 development started


## changelog

### @PROJECT_VERSION_DATE@ @LIBRARY_NAME@-@PROJECT_VERSION@

* now depending on [libsex][]-3.1.0

### 2011-08-18 @LIBRARY_NAME@-0.1.0

First public release. Requires libsex-3.0.0.


## dependencies

* [libsex][]
* CMake
* CppUnit (unit testing, optional)
* Doxygen (API documentation, optional)


## bugs

None known.


## authors

* Andreas Waidler <arandes@programmers.at>


## repo

* git://repo.or.cz/@LIBRARY_NAME@.git
* <http://www.repo.or.cz/w/@LIBRARY_NAME@.git>


## homepage

* <http://programmers.at/work/on/@LIBRARY_NAME@>


## documentation

* <http://programmers.at/work/on/@LIBRARY_NAME@/doc>


## download

* <http://programmers.at/work/on/@LIBRARY_NAME@/@LIBRARY_NAME@-@PROJECT_VERSION@.tar.gz>
* <http://programmers.at/work/on/@LIBRARY_NAME@/@LIBRARY_NAME@-0.1.0.tar.gz>


## license

Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>

Permission to use, copy, modify, and/or distribute this
software for any purpose with or without fee is hereby
granted, provided that the above copyright notice and this
permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.

[libsex]: http://programmers.at/work/on/libsex
