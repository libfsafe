#include <libfsafe/AssertionFailure.hxx>
#include <libsex/define.hxx>

LIBSEX_DEFINE(
	libsex::Exception,
	libfsafe,
	AssertionFailure,
	"Condition '%s' violated.")
