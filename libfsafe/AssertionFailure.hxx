/**
 * @file
 *
 * Contains declaration of @ref libfsafe::AssertionFailure.
 */

#ifndef LIBFSAFE_ASSERTIONFAILURE_HXX
#define LIBFSAFE_ASSERTIONFAILURE_HXX

#include <libsex/declare.hxx>
#include <libsex/Exception.hxx>

/**
 * @class libfsafe::AssertionFailure
 *
 * Indicates that an assertion failed.
 *
 * Superclass of all exceptions thrown by assertions.
 */
namespace libfsafe
{
	class AssertionFailure;
}

LIBSEX_DECLARE(libsex::Exception, libfsafe, AssertionFailure)

#endif
