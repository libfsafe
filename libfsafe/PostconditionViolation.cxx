#include <libfsafe/PostconditionViolation.hxx>
#include <libsex/define.hxx>

LIBSEX_DEFINE(
	libfsafe::AssertionFailure,
	libfsafe,
	PostconditionViolation,
	"Postcondition '%s' violated.")
