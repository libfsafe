/**
 * @file
 *
 * Contains declaration of @ref libfsafe::PostconditionViolation
 */

#ifndef LIBFSAFE_POSTCONDITIONVIOLATION_HXX
#define LIBFSAFE_POSTCONDITIONVIOLATION_HXX

#include <libsex/declare.hxx>
#include <libfsafe/AssertionFailure.hxx>

/**
 * @class libfsafe::PostconditionViolation
 *
 * Indicates that a caller did not fulfill its part
 * of the contract.
 *
 * Inherits from @ref AssertionFailure.
 */
namespace libfsafe
{
	class PostconditionViolation;
}

LIBSEX_DECLARE(
	libfsafe::AssertionFailure,
	libfsafe,
	PostconditionViolation)

#endif
