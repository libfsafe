#include <libfsafe/PreconditionViolation.hxx>
#include <libsex/define.hxx>

LIBSEX_DEFINE(
	libfsafe::AssertionFailure,
	libfsafe,
	PreconditionViolation,
	"Precondition '%s' violated.")
