/**
 * @file
 *
 * Contains declaration of @ref libfsafe::PreconditionViolation
 */

#ifndef LIBFSAFE_PRECONDITIONVIOLATION_HXX
#define LIBFSAFE_PRECONDITIONVIOLATION_HXX

#include <libsex/declare.hxx>
#include <libfsafe/AssertionFailure.hxx>

/**
 * @class libfsafe::PreconditionViolation
 *
 * Indicates that an assertion failed.
 *
 * Superclass of all exceptions thrown by this
 * library and also thrown by the general ASSERT
 * macro.
 */
namespace libfsafe
{
	class PreconditionViolation;
}

LIBSEX_DECLARE(
	libfsafe::AssertionFailure,
	libfsafe,
	PreconditionViolation)

#endif
