#include <libfsafe/UnexpectedException.hxx>
#include <libsex/define.hxx>

LIBSEX_DEFINE(
	libsex::Exception,
	libfsafe,
	UnexpectedException,
	"In '%s':")
