/**
 * @file
 *
 * Contains declaration of @ref
 * libfsafe::UnexpectedException.
 */

#ifndef LIBFSAFE_UNEXPECTEDEXCEPTION_HXX
#define LIBFSAFE_UNEXPECTEDEXCEPTION_HXX

#include <libsex/Exception.hxx>
#include <libsex/declare.hxx>

/**
 * @class libfsafe::UnexpectedException
 *
 * To be thrown when a statement threw an exception that
 * was not anticipated, i.e. wrapping and letting the error
 * bubble up.
 *
 * Inherits from libsex::Exception.:wa
 */
namespace libfsafe
{
	class UnexpectedException;
}

LIBSEX_DECLARE(
	libsex::Exception,
	libfsafe,
	UnexpectedException)

#endif
