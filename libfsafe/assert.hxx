/**
 * @file
 *
 * Contains declarations of assertion macros.
 */

#ifndef LIBFSAFE_ASSERT_HXX
#define LIBFSAFE_ASSERT_HXX

#include <libsex/utility.hxx>
#include <libfsafe/AssertionFailure.hxx>
#include <libfsafe/PreconditionViolation.hxx>
#include <libfsafe/PostconditionViolation.hxx>

/**
 * General assertion macro.
 *
 * There are assertion macros for specific purposes.
 * This one should only be used for internal checks.
 *
 * @param expr Condition to be asserted.
 * @throws libfsafe::AssertionFailure if @a expr is false.
 * @see ASSERT_PRE
 * @see ASSERT_POST
 */
#define ASSERT(expr) \
if (!(expr)) { \
	throw libsex::formatted<libfsafe::AssertionFailure>(__FILE__, __LINE__, #expr); \
}

/**
 * Macro to assert a precondition.
 *
 * To be used to ensure that a method/function is called
 * the right way (e.g. parameter values, internal state).
 *
 * In other words, assert that the callee fulfilled its
 * part of the contract.
 *
 * @param expr Condition to be asserted.
 * @throws libfsafe::PreconditionViolation if @a expr is false.
 * @see ASSERT
 * @see ASSERT_POST
 */
#define ASSERT_PRE(expr) \
if (!(expr)) { \
	throw libsex::formatted<libfsafe::PreconditionViolation>(__FILE__, __LINE__, #expr); \
}

/**
 * Macro to assert a postcondition.
 *
 * To be used to ensure that the result of a
 * function/method is correct, i.e. the callee fulfilled
 * its part of the contract.
 *
 * @param expr Condition to be asserted.
 * @throws libfsafe::PostconditionViolation if @a expr is false.
 * @see ASSERT_PRE
 * @see ASSERT
 */
#define ASSERT_POST(expr) \
if (!(expr)) { \
	throw libsex::formatted<libfsafe::PostconditionViolation>(__FILE__, __LINE__, #expr); \
}

#endif
