/**
 * @file
 *
 * Contains declaration of @ref CHECKPOINT.
 */

#ifndef LIBFSAFE_CHECKPOINT_HXX
#define LIBFSAFE_CHECKPOINT_HXX

#include <libsex/utility.hxx>
#include <libfsafe/UnexpectedException.hxx>

/**
 * Marks an important statement in the program flow.
 *
 * The marked statement is executed. In the default case
 * (the statement being successfull) nothing else will
 * happen. If the statement throws an exception, it will be
 * wrapped into anther exception that contains the location
 * and the statement itsself.
 *
 * Used to create concise backtraces that contain the
 * original exception(s) and the most important (=marked)
 * statements of the current call stack.
 *
 * @param statement The statement to mark and execute.
 * @throws libfsafe::UnexpectedException If statement throws.
 * @see WRAP
 */
#define CHECKPOINT(statement) \
try { \
	statement; \
} catch (libsex::Exception& e) { \
	throw libsex::formatted<libfsafe::UnexpectedException>(e, __FILE__, __LINE__, #statement); \
}

#endif
