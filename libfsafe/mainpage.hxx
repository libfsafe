/**
 * @mainpage
 *
 * @section Overview
 *
 * Usually, all you need is macros. The following ones
 * exist:
 *
 * @li @ref ASSERT
 * @li @ref ASSERT_PRE
 * @li @ref ASSERT_POST
 * @li @ref CHECKPOINT
 *
 *
 * @section Usage Example
 *
 * The unit tests serve as compilable and runnable
 * documentation and examples.
 *
 * Heres the one that is intended as a short tutorial:
 *
 * @c tests/Example.cxx
 * @include Example.cxx
 */
