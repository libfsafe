#include <tests/Assert.hxx>
#include <libfsafe/assert.hxx>

void tests::Assert::passIfTrue()
{
	ASSERT(true);
}

void tests::Assert::failIfFalse()
{
	ASSERT(false);
}

void tests::Assert::testMessage()
{
	try {
		ASSERT(false);
		CPPUNIT_FAIL("false positive");
	} catch (libfsafe::AssertionFailure& e) {
		std::string exp =
			__FILE__ ":17: "
			"Condition 'false' violated.";
		std::string act = e.what();
		CPPUNIT_ASSERT_EQUAL(exp, act);
	}
}
