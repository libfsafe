#ifndef TESTS_ASSERT_HXX
#define TESTS_ASSERT_HXX

#include <libfsafe/AssertionFailure.hxx>
#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class Assert;
}

class tests::Assert : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(tests::Assert);
	CPPUNIT_TEST(passIfTrue);
	CPPUNIT_TEST_EXCEPTION(failIfFalse, libfsafe::AssertionFailure);
	CPPUNIT_TEST(testMessage);
CPPUNIT_TEST_SUITE_END();

public:
	void passIfTrue();
	void failIfFalse();
	void testMessage();
};

#endif
