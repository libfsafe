#include <tests/AssertPost.hxx>
#include <libfsafe/assert.hxx>

void tests::AssertPost::passIfTrue()
{
	ASSERT_POST(true);
}

void tests::AssertPost::failIfFalse()
{
	ASSERT_POST(false);
}

void tests::AssertPost::testMessage()
{
	try {
		ASSERT_POST(false);
		CPPUNIT_FAIL("false positive");
	} catch (libfsafe::AssertionFailure& e) {
		std::string exp =
			__FILE__ ":17: "
			"Postcondition 'false' violated.";
		std::string act = e.what();
		CPPUNIT_ASSERT_EQUAL(exp, act);
	}
}
