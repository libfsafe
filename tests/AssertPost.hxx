#ifndef TESTS_ASSERTPOST_HXX
#define TESTS_ASSERTPOST_HXX

#include <libfsafe/PostconditionViolation.hxx>
#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class AssertPost;
}

class tests::AssertPost : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(tests::AssertPost);
	CPPUNIT_TEST(passIfTrue);
	CPPUNIT_TEST_EXCEPTION(failIfFalse, libfsafe::PostconditionViolation);
	CPPUNIT_TEST(testMessage);
CPPUNIT_TEST_SUITE_END();

public:
	void passIfTrue();
	void failIfFalse();
	void testMessage();
};

#endif
