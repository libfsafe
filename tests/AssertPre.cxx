#include <tests/AssertPre.hxx>
#include <libfsafe/assert.hxx>

void tests::AssertPre::passIfTrue()
{
	ASSERT_PRE(true);
}

void tests::AssertPre::failIfFalse()
{
	ASSERT_PRE(false);
}

void tests::AssertPre::testMessage()
{
	try {
		ASSERT_PRE(false);
		CPPUNIT_FAIL("false positive");
	} catch (libfsafe::AssertionFailure& e) {
		std::string exp =
			__FILE__ ":17: "
			"Precondition 'false' violated.";
		std::string act = e.what();
		CPPUNIT_ASSERT_EQUAL(exp, act);
	}
}
