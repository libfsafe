#ifndef TESTS_ASSERTPRE_HXX
#define TESTS_ASSERTPRE_HXX

#include <libfsafe/PreconditionViolation.hxx>
#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class AssertPre;
}

class tests::AssertPre : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(tests::AssertPre);
	CPPUNIT_TEST(passIfTrue);
	CPPUNIT_TEST_EXCEPTION(failIfFalse, libfsafe::PreconditionViolation);
	CPPUNIT_TEST(testMessage);
CPPUNIT_TEST_SUITE_END();

public:
	void passIfTrue();
	void failIfFalse();
	void testMessage();
};

#endif
