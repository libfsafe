#include <tests/Checkpoint.hxx>
#include <libfsafe/checkpoint.hxx>
#include <libsex/Exception.hxx>
#include <sstream>

void tests::Checkpoint::passIfStatementPasses()
{
	CHECKPOINT((void) true);
}

void tests::Checkpoint::wrapIfStatementThrows()
{
	CHECKPOINT(throw libsex::Exception("Error!"));
}

void tests::Checkpoint::testBacktrace()
{
	try {
		wrapIfStatementThrows();
		CPPUNIT_FAIL("false positive");
	} catch (libsex::Exception& e) {
		std::string exp = __FILE__ ":13: "
			"In 'throw libsex::Exception(\"Error!\")':\n"
			"Error!";
		std::stringstream act;
		e.backtrace(act);
		CPPUNIT_ASSERT_EQUAL(exp, act.str());
	}
}
