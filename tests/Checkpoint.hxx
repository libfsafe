#ifndef TESTS_CHECKPOINT_HXX
#define TESTS_CHECKPOINT_HXX

#include <libfsafe/UnexpectedException.hxx>
#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class Checkpoint;
}

class tests::Checkpoint : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(tests::Checkpoint);
	CPPUNIT_TEST(passIfStatementPasses);
	CPPUNIT_TEST_EXCEPTION(wrapIfStatementThrows, libfsafe::UnexpectedException);
	CPPUNIT_TEST(testBacktrace);
CPPUNIT_TEST_SUITE_END();

public:
	void passIfStatementPasses();
	void wrapIfStatementThrows();
	void testBacktrace();
};

#endif
