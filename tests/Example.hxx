#ifndef TESTS_EXAMPLE_HXX
#define TESTS_EXAMPLE_HXX

#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class Example;
}

class tests::Example : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(tests::Example);
	CPPUNIT_TEST(main);
CPPUNIT_TEST_SUITE_END();

public:
	void main();

protected:
	void initialize();
	void cleanup();
	void run();
	int  calculate(int* ptr);
};

#endif

