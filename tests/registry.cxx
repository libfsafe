#include <cppunit/extensions/HelperMacros.h>

// Registering all unit tests here, to make them
// easier to disable and enable.

#include <tests/Assert.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::Assert);

#include <tests/AssertPre.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::AssertPre);

#include <tests/AssertPost.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::AssertPost);

#include <tests/Checkpoint.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::Checkpoint);

#include <tests/Example.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::Example);
